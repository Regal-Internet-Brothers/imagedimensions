imagedimensions
===============

A small module for the [Monkey programming language](https://github.com/blitz-research/monkey) which provides utilities for loading the dimensions of images (Currently supports PNG, JPEG, and GIF). Useful for console/command-line game-servers; [see 'mojoemulator'](https://github.com/Regal-Internet-Brothers/mojoemulator).
